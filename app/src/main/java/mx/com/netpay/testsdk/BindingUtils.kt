package mx.com.netpay.testsdk

object BindingUtils {
    @JvmStatic
    fun getDecimalRandomNumber() = (1..300).random().toString()

}